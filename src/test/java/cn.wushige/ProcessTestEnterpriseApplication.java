package cn.wushige;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.test.ActivitiRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ProcessTestEnterpriseApplication {

	private RepositoryService repositoryService;
	private RuntimeService runtimeService;
	private TaskService taskService;

	@Rule
	public ActivitiRule activitiRule = new ActivitiRule();

	@Before
	public void setUp() {
		repositoryService = activitiRule.getRepositoryService();
		runtimeService = activitiRule.getRuntimeService();
		taskService = activitiRule.getTaskService();
	}

	@Test
	public void startProcess() throws Exception {
		repositoryService.createDeployment().addClasspathResource("processes/enterprise_application.bpmn").deploy();
		ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().singleResult();
		assertNotNull(processDefinition);
		assertEquals("enterpriseApplication", processDefinition.getKey());
		Map<String, Object> variableMap = new HashMap<>();
		variableMap.put("userId", "wushige");
		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("enterpriseApplication", variableMap);
		assertNotNull(processInstance);
		assertNotNull(processInstance.getId());
		System.out.println(String.format("ProcessInstanceId: %s\nProcessDefinitionId: %s", processInstance.getId(),
				processInstance.getProcessDefinitionId()));
	}
}