package cn.wushige;

import org.activiti.engine.*;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.test.ActivitiRule;
import org.activiti.engine.test.Deployment;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class ProcessTestTestMailTask {

	private RepositoryService repositoryService;
	private RuntimeService runtimeService;
	private TaskService taskService;
	private FormService formService;
	private IdentityService identityService;

	@Rule
	public ActivitiRule activitiRule = new ActivitiRule();

	@Before
	public void setUp() {
		repositoryService = activitiRule.getRepositoryService();
		runtimeService = activitiRule.getRuntimeService();
		taskService = activitiRule.getTaskService();
		formService = activitiRule.getFormService();
		identityService = activitiRule.getIdentityService();
	}

	@Test
	@Deployment(resources = "processes/testMailTask.bpmn")
	public void startProcess() throws Exception {
		Map<String, Object> variables = new HashMap<>();
		variables.put("name", "Jack Woo");
		variables.put("to", "wg0521@qq.com");
		variables.put("from", "wg0521@qq.com");
		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("testMailTask", variables);
		assertNotNull(processInstance.getId());
		System.out.println("id " + processInstance.getId() + " "
				+ processInstance.getProcessDefinitionId());
	}
}